# README

This application's development is based on Docker. To start working on it, install Docker, then 
`docker-compose build`. Create db, load schema, seed `docker-compose run web rake db:create db:schema:load db:seed` and all set up.

Start application by `docker-compose up`.

There are 4 users in the seed: unconfirmed@direkt36.hu, is registered but not confirmed, user@direkt36.hu is a simple user, 
power@direkt36.hu is a power user (can pin and moderate comments) while admin@direkt36.hu has full access to all data in the database.

All the above mentioned users password is `123456` by default.

Client server communications use json format for exchanging data.

The API has the following endpoints:

### Authentication, authorization

Authentication and authorization is provided via a combination of devise, devise-token-auth, omniauth and cancancan gems.

### Topics

`GET /api/topics/:path`

Returns topic details. :path is a uniq string to identify the topic. If there is no topic with the given id, the topic will be created automatically.

`GET /api/topics/:topic_id/pins`

Returns a list of pins for the given topic.

`POST /api/topics/:topic_id/like`

Creates a topic like.

### Contributions

`GET /api/topics/:topic_id/contributions`

Returns a list of first level contributions for the given topic.

`GET /api/topics/:topic_id/contributions/:id`

Returns a list of child contributions for the given topic and contribution.

`POST /api/topics/:topic_id/contributions`

Creates a new contribution for the given topic. If contribution_id param is set, the new contribution will be a child for the given contribution.

`DELETE /api/topics/:topic_id/contributions/:id`

Soft deleting a contribution (moderate).

`PATCH /api/topics/:topic_id/contributions/:contribution_id/restore`

Restores a soft deleted contribution.

`GET /api/topics/:topic_id/contributions/:contribution_id/text`

Returns an editable text reporesentation of the given contribution for further editing before pinning.

`POST /api/topics/:topic_id/contributions/:contribution_id/pin`

Create a pin.

`DELETE /api/topics/:topic_id/pins/:id`

Destroy a pin.

`POST /api/topics/:topic_id/contributions/:contribution_id/like`

Like or remove like for the given contribution.

### Users

`PATCH /api/users/:user_id/block`

Block a user.

`PATCH /api/users/:id`

Set user's role, unblock a user

`GET /api/users`

Returns a list of users.

