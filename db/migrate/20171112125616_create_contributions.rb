class CreateContributions < ActiveRecord::Migration[5.1]
  def change
    create_table :contributions do |t|
      t.text :body, null: false
      t.belongs_to :user, null: false
      t.belongs_to :contribution, null: true
      t.belongs_to :topic, null: false

      t.datetime :deleted_at
      t.timestamps
    end

    add_index :contributions, :deleted_at
  end
end
