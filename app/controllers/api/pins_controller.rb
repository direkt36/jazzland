class Api::PinsController < ApiController
  def pin_params
    params
      .permit(:body, :contribution_id, :topic_id)
  end

  def destroy
    pin = Pin.find params[:id]
    raise CanCan::AccessDenied unless can? :destroy, pin
    pin.destroy
  end
end
