Rack::Attack.safelist('allow from referers') do |req|
  req.path.match(/^api/) && (
    req.referer.match(/^http:\/\/localhost:3000\//) ||
    req.referer.match(/^https:\/\/jazzland.herokuapp.com\//) ||
    req.referer.match(/^https:\/\/zetland.direkt36.hu\//) ||
    req.referer.match(/^https:\/\/jazzland.direkt36.hu\//) ||
    req.referer.match(/^http:\/\/direkt36.biroda.hu\//)
  )
end

Rack::Attack.throttle('req/ip', :limit => 20, :period => 1.second) do |req|
  req.ip
end
#
# Rack::Attack.throttle('logins/email', :limit => 6, :period => 60.seconds) do |req|
#   req.params['email'] if req.path == '/login' && req.post?
# end
