class Api::TopicsController < ApiController
  def show
    ::Current.user = current_user
    @topic = Topic.find_or_create_by path: params[:id]
    json_response @topic,
      include: [
        :current_user_likes,
        pins: {
          only: pin_fields,
          include: [
            contribution: {
              only: contribution_fields,
              include: [
                user: {
                  methods: [:avatar_url, :contributions_count],
                  only: user_fields
                }
              ]
            }
          ]
        }
      ],
      only: topic_fields
  end

  def like
    raise CanCan::AccessDenied unless current_user
    topic = Topic.find params[:topic_id]
    user_like = topic.topic_user_likes.find_by(user_id: current_user.id)
    unless user_like
      user_like = topic.topic_user_likes.new(user_id: current_user.id)
      user_like.save! if can? :create, user_like
    end
    topic.recalculate_likes
    topic.save!
    likes_count = { likes: topic.likes_count, user_like: user_like }
    json_response likes_count
  end

  def pins
    @topic = Topic.find params[:topic_id]
    json_response @topic.pins,
      only: pin_fields
  end

  def new
    response = { authenticity_token: form_authenticity_token }
    if current_user
      response.merge! user: {
          id: current_user.id,
          name: current_user.name,
          role: current_user.role
        }
    else
      response.merge! user: nil
    end
    logger.info response
    json_response response
  end
end
