class Topic < ApplicationRecord
  audited
  acts_as_paranoid

  has_many :contributions
  has_many :pins, through: :contributions

  before_save :update_counts

  has_many :topic_user_likes, dependent: :destroy

  def recalculate_likes
    self.likes_count = topic_user_likes.count
  end

  private

  def update_counts
    self.contributions_count = contributions.first_level.count
    self.cumulated_contributions_count = \
      contributions.first_level.sum(:cumulated_contributions_count) +
      contributions_count
    self.pins_count = pins.count
  end

  def current_user_likes
    return [] unless ::Current.user
    topic_user_likes.where(user_id: ::Current.user.id)
  end
end
