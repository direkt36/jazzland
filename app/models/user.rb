class User < ApplicationRecord
  # Include default devise modules.
  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable, :validatable,
          :confirmable, :omniauthable, omniauth_providers: %i[facebook google_oauth2]
  include DeviseTokenAuth::Concerns::User
  audited except: [
    :tokens, :sign_in_count, :current_sign_in_at, :encrypted_password,
    :reset_password_token, :last_sign_in_at, :current_sign_in_ip,
    :last_sign_in_ip, :confirmation_token, :unlock_token
  ]
  acts_as_paranoid

  alias_attribute :nickname, :name
  alias_attribute :image, :avatar
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable, :confirmable#, :lockable

  has_many :contributions
  has_many :pins

  has_many :user_likes, dependent: :destroy
  has_many :topic_user_likes, dependent: :destroy
  # has_many :liked_contributions, through: :user_likes, class_name: 'Contribution'

  validates :name, presence: true

  after_find do
    self.tokens = {} if tokens == '{}'
  end

  before_save :render_html

  has_attached_file :avatar,
    styles: { normal: "300x300#" },
    default_url: "/images/:style/missing.png"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/

  alias_attribute :file, :avatar

  def token_validation_response
    self.as_json(
      methods: [:avatar_url, :contributions_count],
      only: [:name, :bio, :bio_html, :role, :id]
    )
  end

  def admin?
    role == 'admin'
  end

  def power_user?
    role == 'power'
  end

  def user?
    role != 'admin' && role != 'power'
  end

  def avatar_url
    avatar.url(:normal)
  end

  def recalculate_contributions_count
    contributions_count = contributions.count
  end

  def render_html
    markdown = Redcarpet::Markdown.new(
      Redcarpet::Render::HTML.new(
        escape_html: true,
        safe_links_only: true,
        hard_wrap: true,
        link_attributes: {
          target: '_blank'
        }
      ),
      autolink: true
    )
    self.bio_html = markdown.render(bio || 'Nincs megadva')
  end
end
