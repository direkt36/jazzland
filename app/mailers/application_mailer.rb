class ApplicationMailer < ActionMailer::Base
  default from: 'ne-valaszolj@direkt36.hu'
  layout 'mailer'
end
