app.directive("contributor", ['$rootScope', '$sce',
  function($rootScope, $sce) {
  return {
    restrict: "E",
    templateUrl: 'contributor.html',
    link: function( scope){
      scope.sanitizemodal = function(){
        console.log('wtf')
        scope.modalUser.sce_bio = $sce.trustAsHtml(scope.modalUser.bio_html);
      }
    }
  };
}]);
