class CreateTopicUserLikes < ActiveRecord::Migration[5.1]
  def change
    create_table :topic_user_likes do |t|
      t.integer :user_id
      t.integer :topic_id
      t.timestamps
    end

    add_column :topics, :likes_count, :integer, default: 0
  end
end
