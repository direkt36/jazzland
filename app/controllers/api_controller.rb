class ApiController < ActionController::Base
  include DeviseTokenAuth::Concerns::SetUserByToken
  # protect_from_forgery with: :exception

  # before_action :set_file_as_avatar

  protected

  rescue_from CanCan::AccessDenied do |exception|
    head :forbidden, content_type: 'text/html'
  end

  private

  # def set_file_as_avatar
  #   return unless params[:file]
  #   params[:avatar] = params[:file]
  # end

  def json_response(object, options = {})
    options[:status] ||= :ok
    logger.info options
    render options.merge json: object
  end

  def user_fields
    [:name, :bio_html, :role, :id]
  end

  def contribution_fields
    [:id, :body_html, :contribution_id, :deleted_at, :topic_id, :contributions_count,
      :cumulated_contributions_count, :likes_count, :created_at]
  end

  def pin_fields
    [:id, :body_html, :contribution_id, :created_at]
  end

  def topic_fields
    [:id, :contributions_count, :cumulated_contributions_count, :likes_count]
  end
end
