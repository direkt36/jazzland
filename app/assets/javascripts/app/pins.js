app.directive("pins", ['$http', '$sce', '$rootScope',
  function($http, $sce, $rootScope) {
  return {
    restrict: "E",
    templateUrl: 'pins.html',
    link: function (scope) {
      scope.sanitize = function(pin){
        pin.sce_html = $sce.trustAsHtml(pin.body_html);
      }

      scope.deletePin = function(pin){
        $http
          .delete(scope.api_url + '/topics/' + scope.topic.id + '/pins/' + pin.id)
          .then(function(response){
            var a = scope.topic.pins;
            a.splice(a.indexOf(pin), 1);
            scope.topic.pins = a;
          });
      }

    }
  };
}]);
