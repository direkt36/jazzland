class CreateTopics < ActiveRecord::Migration[5.1]
  def change
    create_table :topics do |t|
      t.string :path, null: false

      t.datetime :deleted_at
      t.timestamps
    end

    add_index :topics, :deleted_at
  end
end
