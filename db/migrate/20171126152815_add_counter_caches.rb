class AddCounterCaches < ActiveRecord::Migration[5.1]
  def change
    add_column :contributions, :contributions_count, :integer, default: 0
    add_column :contributions, :cumulated_contributions_count, :integer, default: 0

    add_column :topics, :contributions_count, :integer, default: 0
    add_column :topics, :cumulated_contributions_count, :integer, default: 0

    add_column :topics, :pins_count, :integer, default: 0
  end
end
