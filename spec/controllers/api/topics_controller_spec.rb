require 'rails_helper'
require 'spec_helper'

RSpec.describe Api::TopicsController, type: :controller do
  include Devise::Test::ControllerHelpers

  describe "Show Contributions" do
    it "returns http status 200" do
      get :show, params: { id: 1 }
      expect(response).to have_http_status(:ok)
    end
  end

  describe "Show Pins" do
    it "returns http status 200" do
      get :pins, params: { topic_id: 1 }
      expect(response).to have_http_status(:ok)
    end
  end
end
