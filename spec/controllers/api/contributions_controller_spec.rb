require 'rails_helper'
require 'spec_helper'

RSpec.describe Api::ContributionsController, type: :controller do
  include Devise::Test::ControllerHelpers

  describe "Show subcontributions" do
    it "returns http status 200" do
      get :show, params: { id: 1, topic_id: 1 }
      expect(response).to have_http_status(:ok)
    end

    it "returns a list of contributions" do
      get :show, params: { id: 1, topic_id: 1 }
      expect(response).to have_http_status(:ok)
    end
  end

  describe "Create contribution" do
    # it "returns http status 201 if admin" do
    #   sign_in User.find_by email: 'admin@direkt36.hu'
    #   request.headers.merge! resource.create_new_auth_token
    #
    #   post :create,
    #     params: {
    #       contribution_id: 1,
    #       body: 'Some text',
    #       topic_id: 1
    #     }
    #   expect(response).to have_http_status(:created)
    # end
    #
    # it "returns http status 201 if power user" do
    #   sign_in User.find_by email: 'power@direkt36.hu'
    #   post :create,
    #     params: {
    #       contribution_id: 1,
    #       body: 'Some text',
    #       topic_id: 1
    #     }
    #   expect(response).to have_http_status(:created)
    # end
    #
    # it "returns http status 201 if simple user" do
    #   sign_in User.find_by email: 'user@direkt36.hu'
    #
    #   post :create,
    #     params: {
    #       contribution_id: 1,
    #       body: 'Some text',
    #       topic_id: 1
    #     }
    #   expect(response).to have_http_status(:created)
    # end
    #
    # it "returns http status 401 if not logged in" do
    #   post :create,
    #     params: {
    #       contribution_id: 1,
    #       body: 'Some text',
    #       topic_id: 1
    #     }
    #   expect(response).to have_http_status(:unauthorized)
    # end
  end
end
