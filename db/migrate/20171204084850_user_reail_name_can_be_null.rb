class UserReailNameCanBeNull < ActiveRecord::Migration[5.1]
  def change
    change_column :users, :real_name, :string, null: true
  end
end
