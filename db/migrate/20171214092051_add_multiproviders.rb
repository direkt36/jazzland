class AddMultiproviders < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :providers, :jsonb, default: {}
  end
end
