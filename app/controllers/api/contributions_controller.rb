class Api::ContributionsController < ApiController
  def show
    ::Current.user = current_user
    @contribution = Contribution.with_deleted.find(params[:id]).contributions
    @contribution = @contribution.with_deleted if params[:filter] == 'show_deleted'
    json_response @contribution
      .includes(:user)
      .exclude(params[:exclude] || [])
      .popular
      .limited,
        include: [ :current_user_likes, user: {
          methods: [:avatar_url, :contributions_count],
          only: user_fields }
        ],
        only: contribution_fields
  end

  def index
    ::Current.user = current_user
    @topic = Topic.find params[:topic_id]
    @response = @topic.contributions
    @response = @response.with_deleted if params[:filter] == 'show_deleted' && ::Current.user.admin?
    @response = @response.includes(:user)
      .exclude(params[:exclude] || [])
      .first_level
      .popular
      .limited

    json_response @response,
      include: [ :current_user_likes, user: {
        methods: [:avatar_url, :contributions_count],
        only: user_fields }
      ],
      only: contribution_fields
  end

  def create
    contribution = Contribution.new(contribution_params)
    if can? :create, contribution
      contribution.user = current_user
      contribution.save!
      json_response contribution,
        only: contribution_fields,
        include: [ :current_user_likes, user: {
          methods: [:avatar_url, :contributions_count],
          only: user_fields } ],
        status: :created
    else
      json_response 'Not permitted', status: :unauthorized
    end
  end

  def like
    raise CanCan::AccessDenied unless current_user
    contribution = Contribution.find params[:contribution_id]
    user_like = contribution.user_likes.find_by(user_id: current_user.id)
    dir = nil
    unless user_like
      user_like = contribution.user_likes.new(user_id: current_user.id)
      user_like.save! if can? :create, user_like
      dir = :up
    else
      user_like.destroy! if can? :destroy, user_like
      dir = :down
    end
    contribution.recalculate_likes
    contribution.save!
    likes_count = { likes: contribution.likes_count, user_like: user_like, dir: dir }
    json_response likes_count
  end

  def pin
    raise CanCan::AccessDenied unless current_user
    contribution = Contribution.find params[:contribution_id]
    pin = contribution.pins.first
    pin = Pin.new(
      user_id: current_user.id,
      contribution_id: contribution.id
    ) unless pin
    raise CanCan::AccessDenied unless current_user.id == pin.user.id
    pin.body = params[:text]
    if can? :create, pin
      pin.save!
    else
      raise CanCan::AccessDenied
    end
    json_response pin,
      only: pin_fields,
      include: [
        contribution: {
          only: contribution_fields,
          include: [
            user: {
              methods: [:avatar_url, :contributions_count],
              only: user_fields }
          ]
        }
      ]
  end

  def text
    raise CanCan::AccessDenied unless current_user
    contribution = Contribution.find params[:contribution_id]
    json_response contribution, only: :body
  end

  def destroy
    contribution = Contribution.find params[:id]
    if can? :destroy, contribution
      contribution.destroy
      json_response contribution, status: 204
    else
      raise CanCan::AccessDenied
    end
  end

  def restore
    contribution = Contribution.with_deleted.find params[:contribution_id]
    if can? :destroy, contribution
      contribution.restore
      json_response contribution, status: 204
    else
      raise CanCan::AccessDenied
    end
  end

  private

  def contribution_params
    params
      .permit(:contribution_id, :body, :body_html, :topic_id)
  end
end
