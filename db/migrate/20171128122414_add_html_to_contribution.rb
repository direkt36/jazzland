class AddHtmlToContribution < ActiveRecord::Migration[5.1]
  def change
    add_column :contributions, :body_html, :text
  end
end
