app.controller('adminController', ['$rootScope', '$scope', '$http', '$auth', '$location', '$window',
  function($rootScope, $scope, $http, $auth, $location, $window) {
    $scope.showUpload = false;
    $scope.showDownload = false;
    $scope.showGrid = true;
    $scope.showErrors = false;
    $scope.errors = [];
    $scope.errorText = '';

    $scope.linesCount = 0;
    $scope.errorCount = 0;
    $scope.doneCount = 0;
    $scope.gridOptions = {
      onRegisterApi: function(gridApi){ $scope.gridApi = gridApi;},
      enableFiltering: true,
      appScopeProvider: $scope,
      columnDefs: [
        // default
        { field: 'name', headerCellClass: $scope.highlightFilteredHeader },
        { field: 'email', headerCellClass: $scope.highlightFilteredHeader },
        { field: 'role',
          headerCellClass: $scope.highlightFilteredHeader,
          cellTemplate: '<div>{{row.entity.role}} '+
          '<a ng-click="grid.appScope.setRole(\'user\', row.entity)">user</a>/'+
          '<a ng-click="grid.appScope.setRole(\'power\', row.entity)">power</a>/'+
          '<a ng-click="grid.appScope.setRole(\'admin\', row.entity)">admin</a>/'+
          '<a ng-click="grid.appScope.setRole(\'blocked\', row.entity)">block</a>'+
          '</div>'
        }

      ]
    };

    $scope.setRole = function(role, user){
      console.log(role, user);
      $http
        .patch($scope.api_url + '/users/' + user.id, {
          role: role
        }).then(function(response){
          user.role = role;
          $scope.gridApi.core.refresh();
        })
    }

    $scope.loadUsers = function(){
      $http
        .get($scope.api_url + '/users')
        .then(function(response){
          console.log(response);
          $scope.gridOptions.data = response.data;
        });
    }

    $scope.uploadUsers = function(){
      $scope.showUpload = !$scope.showUpload;
      $scope.showDownload = false;
      if($scope.showUpload) $scope.showGrid = false;
    }

    $scope.downloadUsers = function(){
      $scope.showDownload = !$scope.showDownload;
      $scope.showUpload = false;
    }

    $scope.uploadUser = function(){
      var user = $scope.uploading.pop();

      user.password = Math.random().toString(36).slice(-8);
      user.password_confirmation = user.password;
      console.log(user);
      $auth.submitRegistration(user)
        .then(function(response){
          $scope.doneCount ++;
        }, function(response){
          $scope.errorCount ++;
          // $scope.errors.push({user: user, errors: response.data.errors.full_messages});
          $scope.errorText += user.name + ' - ' + user.email + ' - ' +
            response.data.errors.full_messages.join(', ') + "\n";
        })

      if($scope.uploading.length > 0){
        $scope.uploadUser();
      }
      else{
        $scope.showErrors = true;
      }
    }

    $scope.createDownloadText = function(){
      $scope.downloadText = '';
      $scope.gridOptions.data.forEach(function(v, i){
        var data = [];
        data.push(v.email);
        data.push(v.name);
        data.push(v.role);
        $scope.downloadText += data.join(',') + "\n";
      })
      $scope.downloadText = "data:attachment/csv," + encodeURIComponent($scope.downloadText);
    }

    $scope.loadUsers();
  }
])
