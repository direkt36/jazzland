class UserMailer < ApplicationMailer
  def block(user)
    mail(to: user.email, subject: 'A felhasználód blokkoltuk')
  end
end
