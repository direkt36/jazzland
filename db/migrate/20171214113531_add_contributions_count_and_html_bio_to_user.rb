class AddContributionsCountAndHtmlBioToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :contributions_count, :integer, default: 0
    add_column :users, :bio_html, :text, default: ''
  end
end
