class CreatePins < ActiveRecord::Migration[5.1]
  def change
    create_table :pins do |t|
      t.datetime :deleted_at
      t.text :body, null: false
      t.belongs_to :user, null: false
      t.belongs_to :contribution, null: false
      t.timestamps
    end

    add_index :pins, :deleted_at
  end
end
