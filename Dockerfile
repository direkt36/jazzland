FROM ruby:2.4.2

RUN curl -sL http://deb.nodesource.com/setup_8.x | bash -
RUN curl -sS http://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb http://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs yarn imagemagick
RUN yarn global add @angular/cli

RUN ng set --global packageManager=yarn

RUN yarn

RUN mkdir /symbio
RUN mkdir /bundle

WORKDIR /symbio

ADD Gemfile /symbio/Gemfile
ADD Gemfile.lock /symbio/Gemfile.lock

ENV BUNDLE_PATH=/bundle
ENV GEM_HOME=/bundle

RUN bundle install
ADD . /symbio
