require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module JazzLand
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.

    # config.load_defaults 5.1
    config.action_mailer.delivery_method = :mailgun
    config.action_mailer.mailgun_settings = {
      api_key: 'key-c1969d4b587122e94317410d3723023f',
      domain: 'mg.direkt36.hu',
    }


    # config.action_mailer.delivery_method = :smtp
    # config.action_mailer.smtp_settings = {
    #   address:              'smtp.gmail.com',
    #   port:                 587,
    #   domain:               'example.com',
    #   user_name:            'direkt36.komment@gmail.com',
    #   password:             '629cLoFXv1U12d',
    #   authentication:       'plain',
    #   enable_starttls_auto: true  }


    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.middleware.insert_before 0, Rack::Cors do
      allow do
        origins '*'
        resource '*', :headers => :any, :methods => :any,
          :expose  => ['access-token', 'expiry', 'token-type', 'uid', 'client']
      end
    end
  end
end
