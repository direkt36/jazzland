class Pin < ApplicationRecord
  audited
  acts_as_paranoid

  belongs_to :contribution, touch: true
  belongs_to :user

  before_save :render_html

  def render_html
    markdown = Redcarpet::Markdown.new(
      Redcarpet::Render::HTML.new(
        escape_html: true,
        safe_links_only: true,
        hard_wrap: true,
        link_attributes: {
          target: '_blank'
        }
      ),
      autolink: true
    )
    self.body_html = body.gsub(/(https?:\/\/.*\.(?:png|jpg))/, '![](\1)')
    self.body_html = markdown.render(body_html)
  end
end
