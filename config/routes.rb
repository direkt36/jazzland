Rails.application.routes.draw do
  # devise_for :users, ActiveAdmin::Devise.config
  # ActiveAdmin.routes(self)
  root to: 'home#index' # if Rails.env.development?
  mount_devise_token_auth_for 'User', at: 'api/auth'

  namespace :api do
    resources :topics, only: [:show] do
      get 'pins', controller: 'topics', action: 'pins'
      post 'like', controller: 'topics', action: 'like'
      resources :contributions, only: [:show, :create, :index, :destroy] do
        post 'like', controller: 'contributions', action: 'like'
        post 'restore', controller: 'contributions', action: 'restore'
        post 'pin', controller: 'contributions', action: 'pin'
        get 'text', controller: 'contributions', action: 'text'
      end
      resources :pins, only: [:create, :destroy]
    end
    resources :users, only: [:index, :show, :create, :update] do
      patch 'block', controller: 'users', action: 'block'
    end
  end
end
