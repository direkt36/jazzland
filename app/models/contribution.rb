class Contribution < ApplicationRecord
  audited
  acts_as_paranoid

  before_save :update_counts, :render_html
  after_save :update_parents, :recalculate_user_contributions
  after_destroy :update_parents

  belongs_to :user

  belongs_to :contribution, optional: true, touch: true
  belongs_to :topic, touch: true

  has_many :contributions
  has_many :pins

  has_many :user_likes, dependent: :destroy
  has_many :users, through: :user_likes

  validates :body, presence: true

  scope :first_level, -> () { where contribution_id: nil }
  scope :popular, -> () do
    order('10 - (EXTRACT(EPOCH FROM age(created_at)) / 6000) + ' +
      'cumulated_contributions_count + likes_count DESC')
  end

  scope :exclude, -> (ids) do
    if ids && !ids.empty?
      where('id not in (?)', ids)
    else
      where('1=1')
    end
  end

  scope :limited, -> () { limit(30) }

  def current_user_likes
    return [] unless ::Current.user
    user_likes.where(user_id: ::Current.user.id)
  end

  def first_level_contributions
    contributions.first_level
  end

  def recalculate_likes
    self.likes_count = user_likes.count
  end

  private

  def update_counts
    self.contributions_count = contributions.count
    self.cumulated_contributions_count = \
      contributions.sum(:cumulated_contributions_count) +
      contributions_count
  end

  def update_parents
    contribution.save! if contribution
    topic.save! if topic
  end

  def recalculate_user_contributions
    user.recalculate_contributions_count
    user.save!
  end

  def render_html
    markdown = Redcarpet::Markdown.new(
      Redcarpet::Render::HTML.new(
        escape_html: true,
        safe_links_only: true,
        hard_wrap: true,
        link_attributes: {
          target: '_blank'
        }
      ),
      autolink: true
    )
    # if user.role == 'power' || user.role == 'admin'
      self.body_html = body.gsub(/(https?:\/\/.*\.(?:png|jpg))/, '![](\1)')
    # end
    self.body_html = markdown.render(body_html)
  end
end
