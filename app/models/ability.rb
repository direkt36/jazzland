class Ability
  include CanCan::Ability

  def initialize(user)
    @user = user
    if user && user.admin?
      admin_abilities
    elsif user && user.power_user?
      power_user_abilities
    elsif user && user.user?
      user_abilities
    else
      public_abilities
    end
  end

  def public_abilities
    can :read, Topic
    can :read, Contribution
    can :read, Pin
    can :read, UserLike
    can :create, User
  end

  def user_abilities
    can :read, Topic
    can :read, Contribution
    can :read, Pin
    can :read, UserLike
    can :manage, Contribution, user: @user
    can :create, Contribution
    can :manage, User, id: @user.id
    can :create, UserLike
    can :destroy, UserLike, user: @user
    can :create, TopicUserLike
  end

  def power_user_abilities
    user_abilities
    can :manage, Pin, user: @user
    can :manage, Contribution
  end

  def admin_abilities
    can :manage, :all
  end
end
