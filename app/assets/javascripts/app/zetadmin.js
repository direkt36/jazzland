app.directive("zetadmin", ['$http', '$location',
  function($http, $location) {
  return {
    restrict: "E",
    templateUrl: 'zetdmin.html',
    link: function(){
    }
  };
}]);

app.directive("csvRead", ['$http', '$location',
  function($http, $location) {
  return {
    templateUrl: 'zetdmin.html',
    link: function(scope, element){
      document.getElementById('uploadFile')
      .addEventListener('change', function (evt) {
        var files = evt.target.files;
        var file = files[0];
        var reader = new FileReader();
        reader.onload = function(event) {
          lines = event.target.result.split("\n");
          scope.$parent.linesCount = lines.length;
          scope.$parent.errorCount = 0;
          scope.$parent.doneCount = 0;
          users = [];
          lines.forEach(function(v, i){
            user = v.split(',');
            users.push({
              name: user[1],
              email: user[0],
              role: user[2]
            })
          })
          scope.$parent.uploading = users;
          scope.$parent.uploadUser();
        }
        reader.readAsText(file)
      }, false);
    }
  };
}]);
//
// document.getElementById('file').addEventListener('change', readFile, false);
//
