class Api::UsersController < ApiController
  def block
    user = User.find params[:user_id]
    authorize! :update, user
    user.role = 'blocked'
    user.save!
    UserMailer.block(user).deliver_now
    json_response 'blocked', status: 204
  end

  def index
    @users = User.all
    raise CanCan::AccessDenied if !current_user || current_user.role != 'admin'
    json_response @users, only: user_fields.push(:email)
  end

  def update
    user = User.find params[:id]
    raise CanCan::AccessDenied if !current_user || current_user.role != 'admin'
    user.role = params[:role]
    user.save!
    UserMailer.block(user).deliver_now
    json_response @users, only: user_fields.push(:email)
  end
end
