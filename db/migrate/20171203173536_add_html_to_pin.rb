class AddHtmlToPin < ActiveRecord::Migration[5.1]
  def change
    add_column :pins, :body_html, :text
  end
end
