app.directive('ngConfirmClick', [
    function(){
        return {
            link: function (scope, element, attr) {
                var msg = attr.ngConfirmClick || "Are you sure?";
                var clickAction = attr.confirmedClick;
                element.bind('click',function (event) {
                    if ( window.confirm(msg) ) {
                        scope.$eval(clickAction)
                    }
                });
            }
        };
}])

app.controller('AuthenticationController',
  ['$rootScope', '$scope', '$location',
  '$timeout', '$auth', '$http', '$q', 'Upload', '$window',
  function(
    $rootScope, $scope, $location,
    $timeout, $auth, $http, $q, Upload, $window) {

  $scope.signupForm = {};
  $scope.avatarForm = {};
  $scope.showAdminUsers = false;
  $scope.omniuser = function(){
    if (Object.keys($scope.user.providers).length > 0)
      return true;
    else
      return false;
  }

  $scope.showUsers = function(){
    $scope.showAdminUsers = !$scope.showAdminUsers;
  }

  window.validateUser = function() {
    return $auth.validateUser();
  };

  $scope.login = function(data){
    $auth.submitLogin(data).then(function(response){
      $rootScope.user = response;
    }, function(response){
      data.errors = response.errors;
    });
  }

  $scope.logout = function(){
    $auth.signOut().then(function(){
      $rootScope.user = null;
      $scope.showUserOptions = false;
    });
  }

  $scope.profile = function(){
    if ($scope.showUserOptions == true) {
      $scope.showUserOptions = false;
    } else {
      $scope.showUserOptions = true;
    }
  }

  $scope.signupModal = function(){
    $scope.$parent.showRegModal = true;
    $scope.$parent.modalOk = $scope.signup;
  }

  $scope.signup = function(){
    var data = $scope.signupForm;
    $scope.$parent.showRegModal = false;
    $scope.signupForm.errors = null;
    $scope.signupForm.notes = null;
    data.confirm_success_url =  $location.url();
    $auth.submitRegistration(data).then(function(response){
      $rootScope.user = null;
      $scope.signupForm.notes = [
        'A regisztráció sikerült. Küldtünk egy levelet a megadott email címre, ' +
        'kattints a levélben kapott linkre, hogy aktiváld a felhasználódat!'
      ];
      $scope.signupForm.hide = true;
    }, function(response){
      $scope.signupForm.errors = response.data.errors.full_messages;
    });
  }

  $scope.reset = function(data){
    data.notes = null;
    data.errors = null;
    $auth.requestPasswordReset(data).then(function(response){
      data.notes = [response.data.message];
    }, function(response){
      data.errors = response.data.errors;
    });
  }

  $scope.deleteUser = function(){
    $auth.destroyAccount().then(function(){
      $scope.user = null;
    });
  }

  $scope.changeName = function(data){

    $auth.updateAccount(data).then(function(response){
      data.notes = ['Megváltoztatva!'];
    }, function(response){
      data.errors = response.data.errors.full_messages;
    });
  }

  $scope.changeBio = function(data){
    $auth.updateAccount(data).then(function(response){
      data.notes = ['Megváltoztatva!'];
    }, function(response){
      data.errors = response.data.errors.full_messages;
    });
  }

  $scope.changePassword = function(data){
    $auth.updatePassword(data).then(function(response){
      data.notes = ['Megváltoztatva!'];
    }, function(response){
      console.log('wtf', response)
      data.errors = response.data.errors.full_messages;
    });
  }

  $scope.file_changed = function(element){
    $rootScope.file_to_upload = element.files[0];
  }

  $scope.uploadImage = function(data){
    Upload.upload({
      url: '/api/auth',
      method: 'PATCH',
      file: data.avatar,
      headers: $auth.retrieveData('auth_headers')
    }).then(function(response){
      var key, newHeaders, val, _ref;
      newHeaders = {};
      _ref = $auth.getConfig().tokenFormat;
      for (key in _ref) {
        val = _ref[key];
        if (response.headers(key)) {
          newHeaders[key] = response.headers(key);
        }
      }
      $auth.setAuthHeaders(newHeaders);
      $scope.user.avatar_url = response.data.data.avatar_url;
    });
  }

  $scope.downloadUsers = function(){//
    var url = $auth.getConfig().apiUrl + '/user-list';
    $window.open(url);
  }


  $scope.showResetForm = function(){
    $scope.resetFormEnabled = true;
  }

  $scope.showDeleted = function(){
    $rootScope.commentsFilter = 'show_deleted';
    $scope.showAllComments = true;
  }

  $scope.hideDeleted = function(){
    $rootScope.commentsFilter = 'default';
    $scope.showAllComments = false;
  }

  $scope.showLoginForm = function(){
    $scope.resetFormEnabled = false;
  }

  $scope.googleModal = function(){
    $scope.$parent.showRegModal = true;
    $scope.$parent.modalOk = $scope.googleAuth;
  }

  $scope.facebookModal = function(){
    $scope.$parent.showRegModal = true;
    $scope.$parent.modalOk = $scope.facebookAuth;
  }

  $scope.googleAuth = function(response){
    $auth.authenticate('google');
  }

  $scope.facebookAuth = function(){
    $auth.authenticate('facebook');
  }

  $scope.autoExpand = function(e) {
    var element = typeof e === 'object' ? e.target : document.getElementById(e);
    var scrollHeight = element.scrollHeight -22;
    element.style.height =  scrollHeight + "px";
  };
  function expand() {
    $scope.autoExpand('TextArea');
  }

}]);
app.directive("authenticate", [function() {
  return {
    restrict: "E",
    templateUrl: 'authenticate.html',
    link: function (scope) {

    }
  }
}]);
